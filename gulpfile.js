var gulp = require('gulp');

var PATHS = {
    src: 'src/**/*',
    dist: 'dist/'
};

gulp.task('clean', function (done) {
    var del = require('del');
    del(['dist'], done);
});

gulp.task('copy', function () {
    gulp.src(PATHS.src + '.html')
        .pipe(gulp.dest(PATHS.dist));
});

gulp.task('ts2js', function () {
    var typescript = require('gulp-typescript');
    var tscConfig = require('./tsconfig.json');

    var tsResult = gulp
        .src(PATHS.src + '.ts')
        .pipe(typescript(tscConfig.compilerOptions));

    return tsResult.js.pipe(gulp.dest('dist'));
});

gulp.task('styles', function(){
    var less = require('gulp-less');
    var path = require('path');
    gulp.src(PATHS.src + '.less')
    .pipe(less({
        paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('dist'))
})

gulp.task('play', ['ts2js', 'copy'], function () {
    gulp.watch(PATHS.src + '.ts', ['ts2js']);
    gulp.watch(PATHS.src + '.html', ['copy']);
    gulp.watch(PATHS.src + '.less', ['styles']);

    var webserver = require('gulp-webserver');
    gulp.src('./')
        .pipe(webserver({
            livereload: true,
            open: true,
            port: 9000,
            fallback: 'index.html'
        }));
});

gulp.task('default', ['play']);
