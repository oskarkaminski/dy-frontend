import {Component} from 'angular2/core';
import {ROUTER_DIRECTIVES} from 'angular2/router';
import template from './navbar.component.html!text';
import style from './navbar.component.css!text';

@Component({
  selector: 'navbar',
  template: template,
  styles: [style],
  directives: [ROUTER_DIRECTIVES]
})
export class NavbarComponent {}
