import {Http, Response} from 'angular2/http';
import {Injectable} from 'angular2/core';
import Habit from './habit.model';


@Injectable()
export default class HabitsSvc{

    habits: Habit[];
    fetched: boolean = false;

    constructor(
        private http: Http
    ){}

    fetch(){
        return this.http.get('https://dy-challenges-service.herokuapp.com/challenges/fetch');
    }

    add(item: Habit){
        this.http.post('https://dy-challenges-service.herokuapp.com/challenges/save', JSON.stringify(item))
            .subscribe();
    }

}