import {Component, OnInit} from 'angular2/core';
import InstantInput from 'components/instant-input/instant-input';
import template from './habits.component.html!text';
import HabitsSvc from "./habits.service";
import Habit from "./habit.model";
import Ratings from "../../components/ratings/ratings.component";

@Component({
    selector: 'habits',
    template: template,
    styles: [`
        .rating-panel{
            text-align: right;
        }
    `],
    directives: [InstantInput, Ratings],
    providers: [HabitsSvc]
})
export default class HabitsComponent{
    habits:Habit[] = [];
    type:string = 'habit';

    constructor(public HabitsSvc:HabitsSvc){}

    ngOnInit(){
        this.HabitsSvc.fetch().subscribe((value: Habit[]) => {
            this.habits = value.json();
            console.log({"this.habits": this.habits});
        });
    }

    addHabit(item:any){
        let habit = new Habit(item);
        this.habits.push(habit);
        this.HabitsSvc.add(habit);
    }

    newRate(item, newRate){
        let ratings = item.ratings;
        let today = moment().format('l');
        if(!ratings.length){
            item.ratings[0] = {
                rate: newRate,
                date: today
            }
        } else if(ratings[ratings.length-1].date !== today){
            item.ratings[ratings.length] = {
                rate: newRate,
                date: today
            }
        } else {
            ratings[ratings.length-1].rate = newRate;
        }
        console.log({"habits": this.habits});
    }

    getActualRate(item){
        return item.ratings.length ? item.ratings[item.ratings.length-1].rate : 0;
    }
}