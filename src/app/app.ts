import {Component} from 'angular2/core';
import {bootstrap} from 'angular2/platform/browser';
import {HTTP_PROVIDERS} from 'angular2/http';
import template from './app.html!text';
import HabitsComponent from './habits/habits.component'
import {NavbarComponent} from "./navbar/navbar.component";
import {RouteConfig, ROUTER_PROVIDERS} from "angular2/router";

@Component({
    selector: 'app',
    directives: [HabitsComponent, NavbarComponent],
    template: template,
    providers: [
        ROUTER_PROVIDERS
    ]
})
@RouteConfig([
    {path: '/habits', name: 'Habits', component: HabitsComponent, useAsDefault: true}
])
class App {
}

bootstrap(App, HTTP_PROVIDERS);