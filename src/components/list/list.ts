import {Component, Input} from "angular2/core";
import {NgFor} from "angular2/common";
import template from './list.html!text';
import ListItemComponent from "../list-item/list-item.component";

@Component({
    selector: 'list',
    directives: [NgFor, ListItemComponent],
    template: template
})
export default class List{
    @Input() items = ['qwe', 'asd'];
    @Input() type: string;

    constructor(){
        console.log({"this.type": this.type});
    }
}