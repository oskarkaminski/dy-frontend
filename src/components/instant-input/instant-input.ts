import {Component, Output, EventEmitter} from 'angular2/core';
import {FORM_DIRECTIVES} from 'angular2/common';

@Component({
    selector: 'instant-input',
    directives: [FORM_DIRECTIVES],
    template: `
        <form (ngSubmit)="add()">
            <input type="text" [(ngModel)]="item">
            <button type="submit">Add Habit</button>
        </form>
    `
})
export default class NewHabit{
    @Output() newItemEvent = new EventEmitter<string>();
    item:any = '';

    constructor(){}
    add(){
        this.newItemEvent.next(this.item);
        this.item = '';
    }
}