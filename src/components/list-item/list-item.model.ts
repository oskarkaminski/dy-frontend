interface ISimpleItem{
    title: string
}

interface IListItem<T>{
    (item: T): T
}

export default class ListItem<T>{
    constructor(public item: IListItem<T>){

    }
}