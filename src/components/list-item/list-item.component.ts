import {Component, View, Input} from "angular2/core";
import {NgIf} from "angular2/common";
import ListItem from "./list-item.model";
import template from "./list-item.html!text"
import Ratings from "../ratings/ratings.component";

@Component({
    selector: 'list-item'
})
@View({
    directives: [NgIf, Ratings],
    template: template
})
export default class ListItemComponent {
    @Input() item:ListItem;
    @Input() type:string;

    constructor(){
    }
}