import {Component, Input, Output, EventEmitter} from 'angular2/core';
import template from './ratings.html!text';
import ListItem from "components/list-item/list-item.model";

@Component({
    selector: 'ratings',
    template: template
})
export default class Ratings{
    @Input() rate:number = 0;
    @Output() newRate: EventEmitter = new EventEmitter();
    private range: number[] = [1,2,3,4,5];

    constructor(){

    }

    ngOnInit(){
    }

    clicked(value){
        this.rate = value;
        this.newRate.emit(value);
    }
}
